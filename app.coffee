# Main server.

fs = require 'fs'
path = require 'path'
express = require 'express'
exphbs = require 'express-handlebars'
bodyParser = require 'body-parser'
cookieParser = require 'cookie-parser'

app = new express

app.engine '.hbs', exphbs {
  helpers: {
    toJSON: (obj) -> JSON.stringify obj
  }

  defaultLayout: 'main',
  extname: '.hbs',
  layoutsDir: path.join __dirname, 'views/layouts'
}
app.set 'view engine', '.hbs'
app.set 'views', path.join __dirname, 'views'

app.use bodyParser.urlencoded { extended: false }

app.use cookieParser()


# Show a poll page.
app.get '/poll/:pollId', (req, res) ->
  pollId = req.params['pollId']

  console.log "someone just viewed poll #{pollId}"

  # Find poll information.
  fileName = path.join __dirname, "data/#{pollId}.json"
  fs.readFile fileName, (err, data) ->
    if !err
      try info = JSON.parse data
      catch e
        res.status(500).send '500 internal server error (malformed data)'
        return

      d = new Date
      c = new Date(info['closingDate'])
      if d > c
        # Poll is closed.
        if not info['resultViewableAfterClosed']
          res.status(403).send '403 forbidden (poll is closed)'
          return

        res.redirect "/poll/#{pollId}/result"

      else
        # Poll is open.
        res.render 'poll', {
          title: info['title'],
          author: info['author'],
          authorSite: info['authorSite'],
          description: info['description'],
          closingDate: info['closingDate'],
          limited: info['limited'],
          resultViewable: info['resultViewable'],
          resultViewableAfterClosed: info['resultViewableAfterClosed'],
          image: info['image'],
          canonical: if info['canonicalPrefix'] then info['canonicalPrefix'] + "/poll/#{pollId}" else '',
          options: info['options'],
          clientCanVote: not (info['limited'] and req.cookies["poll_#{pollId}"]),
          clientSessionChoice : req.cookies["poll_#{pollId}"],
        }
    else
      console.log "but poll #{pollId} does not even exist!"
      res.status(404).send '404 poll not found'


# Receive a vote.
app.post '/poll/:pollId', (req, res) ->
  pollId = req.params['pollId']

  choice = req.body['choice']
  console.log "someone just voted #{choice} for poll #{pollId}"

  unless choice?
    # No choice. Try again.
    res.redirect "/poll/#{pollId}"
    return

  # Find poll information.
  fileName = path.join __dirname, "data/#{pollId}.json"
  fs.readFile fileName, (err, data) ->
    if !err
      try info = JSON.parse data
      catch e
        res.status(500).send '500 internal server error (malformed data)'
        return

      d = new Date
      c = new Date(info['closingDate'])
      if d > c
        # Poll is closed.
        res.status(403).send '403 forbidden (poll is closed)'
        return

      if info['limited'] and req.cookies["poll_#{pollId}"]
        res.status(403).send '403 forbidden (already voted before)'
        return
      else
        res.cookie "poll_#{pollId}", choice, { maxAge: 86400 * 1000 }

      # Find poll result.
      fileName = path.join __dirname, "data/#{pollId}.result.json"
      fs.readFile fileName, (err, data) ->
        if err
          # Create new result.
          console.log "creating result for poll #{pollId}"
          o = { 'options': {} }
          for k, v of info['options']
            n = `k == choice ? 1 : 0`
            o['options'][k] = { votes: n }

          fs.writeFile fileName, JSON.stringify(o), (err) ->
            if err
              console.log err

        else
          # Update existing result.
          console.log "updating result for poll #{pollId}"
          try o = JSON.parse data
          catch e
            res.status(500).send '500 internal server error (malformed data)'
            return
          for k, v of o['options']
            if k == choice
              v['votes']++

          fs.writeFile fileName, JSON.stringify(o), (err) ->
            if err
              console.log err

      if info['options'][choice]['next']
        res.redirect info['options'][choice]['next']
      else if info['resultViewable']
        res.redirect "/poll/#{pollId}/result"
      else
        res.redirect "/poll/#{pollId}"

    else
      console.log "but poll #{pollId} does not even exist!"
      res.status(404).send '404 poll not found'


# Show the result of a poll.
app.get '/poll/:pollId/result', (req, res) ->
  pollId = req.params['pollId']

  console.log "someone just viewed result of poll #{pollId}"

  # Find poll information.
  fileName = path.join __dirname, "data/#{pollId}.json"
  fs.readFile fileName, (err, data) ->
    if !err
      try info = JSON.parse data
      catch e
        res.status(500).send '500 internal server error (malformed data)'
        return

      d = new Date
      c = new Date(info['closingDate'])
      if d > c
        # Poll is closed.
        if not info['resultViewableAfterClosed']
          res.status(403).send '403 forbidden (result is not made public)'
          return
      else
        # Poll is open.
        if not info['resultViewable']
          res.status(403).send '403 forbidden (result is not made public)'
          return

      # Find poll result.
      fileName = path.join __dirname, "data/#{pollId}.result.json"
      fs.readFile fileName, (err, data) ->
        if !err
          console.log "found result for poll #{pollId}"
          try o = JSON.parse data
          catch e
            res.status(500).send '500 internal server error (malformed data)'
            return
          res.render 'result', {
            title: info['title'],
            author: info['author'],
            authorSite: info['authorSite'],
            description: info['description'],
            closingDate: info['closingDate'],
            limited: info['limited'],
            resultViewable: info['resultViewable'],
            resultViewableAfterClosed: info['resultViewableAfterClosed'],
            image: info['image'],
            canonical: if info['canonicalPrefix'] then info['canonicalPrefix'] + "/poll/#{pollId}" else '',
            options: info['options'],
            result: o['options'],
            pollClosed: d > c,
          }

        else
          console.log "but poll #{pollId} has no result yet!"
          res.status(404).send '404 result not found'
    else
      console.log "but poll #{pollId} does not even exist!"
      res.status(404).send '404 poll not found'


# Server startup.
do ->
  # Get port from command-line argument.
  port = process.argv.slice(2)[0]
  if !port or isNaN port
    port = 8000
    console.log "using default port: #{port}"
  else
    console.log "using port: #{port}"

  # Get theme name from command-line argument.
  theme = process.argv.slice(2)[1]
  if !theme
    theme = 'assets'
    console.log "using default theme: #{theme}"
  else
    console.log "using theme: #{theme}"
  app.use express.static(theme)

  app.listen port
