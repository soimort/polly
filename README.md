# Polly

[Polly](https://github.com/soimort/polly) is a lightweight, databaseless anonymous poll system, written in CoffeeScript and runs on Express/Node.js.

It's mainly a simple poll system which

* can be hosted on your own server.
* allows voters to make a sequence of single choices, based on their previous answers.
* allows one vote per cookie session/day.

Polly was created for my personal use, so there is no admin page or alike. Create a poll by adding a `.json` file in the `data/` directory, and you're ready to go!

## Usage

As a start, see `data/example.json`. Run the server by

```
$ coffee app.coffee
```

The poll page is at http://localhost:8000/poll/example/.
